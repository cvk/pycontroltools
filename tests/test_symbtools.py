# -*- coding: utf-8 -*-


"""
Some Tests for the symbolic tools (symb_tools)

"""

import sympy as sp
import symb_tools as st

from ipHelp import IPS, ip_syshook

#ip_syshook(1)


def test_colreduced():
    s = sp.Symbol('s')
    A = sp.Matrix([[10, 10], [5*s**2 + 4* s + 3, 7*s**2 + 2* s -11]])

    a1 = 1.52761929831094*s**2 + 1090.22042871763*s + 399.290218920361
    a2 = 1.52812155848171*s**2 + 1090.28880288808*s + 399.290218920361
    A = sp.Matrix([[1000, 1000], [a1, a2]])


    Ac, T = st.get_col_reduced_right(A, s)

    IPS()



def test_trunc_small_values():
    s = sp.Symbol('s')

    y = 1e-12*s + 5

    y2 = st.trunc_small_values(y)

    IPS()


def main():
    #test_colreduced()
    test_trunc_small_values()

if __name__ == "__main__":
    main()