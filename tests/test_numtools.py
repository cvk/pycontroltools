# -*- coding: utf-8 -*-


import numpy as np
import numtools as nt

from ipHelp import IPS


# TODO: rewrite as unittest


def test_mimo_canonical_form():
    A = np.array([[2, 1, 0, 1], [-1, 0, 2, 1], [0, 0, -1, 0], [0, 0, 3, 1]])
    B = np.array([[1, 1, 0, 0], [0, 1, 1, 1]]).T

    L, Q = nt.mimo_controller_canonical_form_trafo(A, B)
    iQ = np.linalg.inv(Q)

    AA = nt.dd(Q, A, iQ)

    AA = nt.np_trunc_small_values(AA)
    IPS()




test_mimo_canonical_form()