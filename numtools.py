# -*- coding: utf-8 -*-

import numpy as np
import scipy as sc
import scipy.integrate

from ipHelp import IPS, Tracer


def dd(*args):
    return reduce(np.dot, args)

# copied from http://mail.scipy.org/pipermail/numpy-discussion/2008-February/031218.html

def matrixrank(A,tol=1e-8):
    s = np.linalg.svd(A,compute_uv=0)

    # if cond == True take 1, else 0:
    return np.sum(  np.where( s>tol, 1, 0 )  )



def zero_crossing_simulation(rhs, zcf, z0, t_values):
    """
    scipy.odeint does not provide a zero crossing function
    naive (and slow) approach

    rhs: rhs function
    zcf: the function whose zerocrossing shall be detected
         takes the state (shape =(n,m) returns shape=n
    z0: initial state
    t_values: time values (up to which the zc event is suspected)
    """

    res = scipy.integrate.odeint(rhs, z0, t_values)

    zerocc = zcf(res) # zero crossing candidate

    test_idx = 2 # ignore numerical noise at the beginning
    try:
        idx0 = np.where(np.sign(zerocc[test_idx:]) != np.sign(zerocc[test_idx]))[0][0]
    except IndexError:
        raise ValueError, "There was no zero crossing"

    idx0+= test_idx

    if zerocc[idx0] == 0:
        idx0+=1


    #IPS()

    t_values = t_values[:idx0]*1 # *1 to prevent referencing

    return t_values, res[:idx0, :]




def cont_continuation(x, stephight, threshold):
    """
    continuous continuation (for 1d-arrays)

    x           .... data


    stephight   ... the expected stephight (e.g 2*pi)

    threshold   .... smallest difference which is considered as a discontinuity
                    which has to be corrected
                    (must be greater than the Lipschitz-Const. of the signal
                     times dt)

    """
    x_d = np.concatenate(  ([0], np.diff(x))  )
    corrector_array = np.sign(x_d) * (np.abs(x_d) > threshold)
    corrector_array = np.cumsum(corrector_array) * -stephight

    return x + corrector_array


def np_trunc_small_values(A, eps = 1e-10):

    res = A*1 # leave the original untouched
    res[np.abs(res) < eps] = 0

    return res

# copied from Mailinglist
# http://mail.scipy.org/pipermail/scipy-user/2008-October/018318.html
def extrema(x, max = True, min = True, strict = False, withend = False):
    """
    This function will index the extrema of a given array x.

    Options:
        max        If true, will index maxima
        min        If true, will index minima
        strict        If true, will not index changes to zero gradient
        withend    If true, always include x[0] and x[-1]

    This function will return a tuple of extrema indexies and values
    """

    # This is the gradient
    from numpy import zeros
    dx = zeros(len(x))
    from numpy import diff
    dx[1:] = diff(x)
    dx[0] = dx[1]

    # Clean up the gradient in order to pick out any change of sign
    from numpy import sign
    dx = sign(dx)

    # define the threshold for whether to pick out changes to zero gradient
    threshold = 0
    if strict:
        threshold = 1

    # Second order diff to pick out the spikes
    d2x = diff(dx)

    if max and min:
        d2x = abs(d2x)
    elif max:
        d2x = -d2x

    # Take care of the two ends
    if withend:
        d2x[0] = 2
        d2x[-1] = 2

    # Sift out the list of extremas
    from numpy import nonzero
    ind = nonzero(d2x > threshold)[0]

    return ind, x[ind]


##############################################################################

# control specific

##############################################################################

def pyc2d(a,b,Ts):

    """
    Algorithmus kopiert von Roberto Bucher

    Begründung: man erweitert den Zustand  xneu = (x,u)
    und sagt u_dot = 0 (weil u=konst.)
    Für das neue System bekommt man die zusammengestzte Matrix und pflückt
    sie hinterher wieder auseinander.

    """


    #a,b,c,d = SYS

    n=np.shape(a)[0]
    nb=np.shape(b)[1]
    #nc=np.shape(c)[0]

    ztmp=np.zeros((nb,n+nb))
    tmp=np.hstack((a,b))
    tmp=np.vstack((tmp,ztmp))

    tt = tmp

    tmp=sc.linalg.expm(tmp*Ts)

    A=tmp[0:n,0:n]
    B=tmp[0:n,n:n+nb]

    return A,B



def test_controlability(A, B):

    n,m = B.shape
    assert A.shape == (n,n)

    Ap = np.eye(n)
    elements = []
    for i in range(n):
        elements.append(np.dot(Ap, B))
        Ap = np.dot(Ap, A)

    C = np.column_stack(elements)

    return matrixrank(C) == n



def matrix_power(A, exp, cache):

    assert exp >= 0 and isinstance(exp, int)

    if not 0 in cache:
        cache[0] = np.eye(A.shape[0])
    if not 1 in cache:
        cache[1]=A

    assert A is cache[1]

    if exp in cache:
        return cache[exp]
    else:
        res = np.dot(A, matrix_power(A, exp-1, cache))
        cache[exp] = res
        return res


def mimo_controller_canonical_form_trafo(A, B):

    test_controlability(A, B)
    n,m = B.shape

    # columns of B:
    bi = list(B.T)

    # successive construction of L:
    # add new cols consisting of A**j*b[i] until linear dependence occurs
    # saving controllability indices

    rank = 0
    L = np.zeros((n, 0))
    cache_A = {}
    contr_idcs = []
    i = 0
    while 1:
        j = 0
        while 1:
            new_col = np.dot( matrix_power(A, j, cache_A), bi[i])
            L = np.column_stack((L, new_col))

            if matrixrank(L) == rank:
                # rank was not augmented
                L = L[:, :-1]
                contr_idcs.append(j)
                break

            # rank was augmented
            rank += 1

            j += 1
        i += 1
        if i == m or rank == n:
            break

    sigma_arr = np.cumsum(contr_idcs)

    assert L.shape == (n,n)
    assert sigma_arr[-1] == n

    iL = np.linalg.inv(L)
    #Tracer()()
    q_list = iL[sigma_arr-1, :] # -1 wegen 0-Indizierung

    Q = np.zeros((0, n))

    for i,d in enumerate(contr_idcs):
        for j in range(d):
            new_row = np.dot(q_list[i], matrix_power(A, j, cache_A))
            Q = np.row_stack((Q, new_row.reshape(-1, n)))


    assert Q.shape == (n,n)


    return L, Q

def null(A, eps=1e-10):
    """
    null-space of a Matrix or 2d-array
    """
    n, m = A.shape
    if n > m :
        return null(A.T, eps).T
        #return null(scipy.transpose(A), eps)
    u, s, vh = sc.linalg.svd(A)
    s=sc.append(s,sc.zeros(m))[0:m]
    null_mask = (s <= eps)
    null_space = sc.compress(null_mask, vh, axis=0)
    return null_space.T

