# -*- coding: utf-8 -*-

# supposed to be run normal python


"""

tools for symbolic treatment

"""

from ipHelp import IPS, ST, ip_syshook


import sympy as sp
import numpy as np
import scipy as sc
import scipy.linalg
import pylab as pl

import symb_tools as st


from symb_tools import lie_deriv, lie_deriv_covf, lie_bracket


sp.var('x1, x2, x3')

# kernel
def null(A, eps=1e-10):
    """
    null-space of a Matrix or 2d-array
    """
    n, m = A.shape
    if n > m :
        return null(A.T, eps).T
        #return null(scipy.transpose(A), eps)
    u, s, vh = sc.linalg.svd(A)
    s=sc.append(s,sc.zeros(m))[0:m]
    null_mask = (s <= eps)
    null_space = sc.compress(null_mask, vh, axis=0)
    return null_space.T


def bilinform(A, x, y):
    """
        returns x.T * A * y
    """

    return np.dot(x.T,  np.dot(A, y) )



def matrix_to_func(A):
    assert A.shape == (2,2)

    def fnc(x,y):
        return  A[0,0]*x + A[0,1]*y , A[1,0]*x + A[1,1]*y

    return fnc

def vfplot2d(xyfnc, xmin=-1, xmax=1, ymin=-1, ymax=1, N=20, mix=0.8,
             scale = 30):
    """
    graphical plot of VF
    """

    x,y = np.linspace(xmin, xmax,N), np.linspace(ymin, ymax, N)
    X,Y = np.meshgrid(x,y)

    U,V = xyfnc(X,Y)

    uv_abs = np.sqrt(U**2+V**2)

    med = np.median(uv_abs)
    uv_scale = (uv_abs + med * mix) / (1+mix)

    U_norm, V_norm = U/uv_abs, V/uv_abs

    U_mix, V_mix = U_norm * uv_scale, V_norm * uv_scale

    pl.quiver(X,Y,U_mix,V_mix, angles='xy', headwidth = 2.1,
              headlength = 3, width=0.002, pivot = 'middle', scale=scale)

def vplot(*vectors, **kwargs):
    """
    plot a set of 2d vectors
    """

    if len(vectors) == 1 and hasattr(vectors[0][0], '__len__'):
        vectors = vectors[0]

    l = kwargs.get('l', 1)
    colors = 'bgrkm'

    for i,v in enumerate(vectors):
        c = colors[i % len(colors)]
        n = np.linalg.norm(v)
        v/=n
        #ST()
        pl.plot([0,v[0]], [0, v[1]], c)


def orthonormal_completion(*vectors, **kwargs):
    eps = kwargs.get('eps', 1e-10)
    n = len(vectors[0].flatten())
    a_vectors = []
    for v in vectors:
        v = v.flatten()
        assert len(v) == n
        a_vectors.append(v)

    orthocomplement=null(np.array(a_vectors).T, eps).T

    # ensure that we have orthonormality
    # even if the original vectors were not
    oc2 = null(orthocomplement, eps).T

    return np.c_[oc2, orthocomplement]




def orthonormal_completion_old(*vectors):
    """
    takes m vectors (assumed to be of 2-norm 1 and orthogonal) and
    creates n-m others
    such that an orthonormal basis arises.

    The Matrix Q with Q.T = Q.inv() is returned
    """

    # !! das ginge auch viel einfacher indem ich die Nullraum-Funktion
    # nutzen würde

    n = len(vectors[0].flatten())
    a_vectors = []
    for v in vectors:
        v = v.flatten()
        assert len(v) == n
        a_vectors.append(v)

    m = len(a_vectors)
    a = np.array(a_vectors).T # cols are our vectors
    assert np.allclose( np.dot(a.T,a), np.eye(m))

    # now create (n-m) vectors that are lineary independent
    I = np.eye(n)
    I[:,:-m]= a

    # !! just an assumption (the opposite should be improbable)
    assert not np.allclose(np.linalg.det(I), 0)


    b_vectors = list(I[:, m:].T)


    # Matrix D collects the dyadic products
    D = np.zeros((n,n))*0
    for v in a_vectors:
        D+= np.dot( v.reshape(n,1), v.reshape(1,n) )

    #ST()
    for w in b_vectors:

        w = w.reshape(n,1)

        v = w - np.dot(D, w)
        v/= np.linalg.norm(v)
        a_vectors.append(v.flatten())

        D+= np.dot( v.reshape(n,1), v.reshape(1,n) )

    assert len(a_vectors) == n

    Q = np.array(a_vectors).T


    assert np.allclose( np.dot(Q, Q.T), np.eye(n) )

    return Q




def nl_acker_test(f, g, args):
    # hier gings um die Studienarbeit von Herrn Paschke
    # !!erstmal nur 2d

    f = sp.Matrix(f)
    g = sp.Matrix(g)

    ad = lie_bracket(-f, g, args)
    pi = st.concat_cols(g, ad)

    en = st.uv(2,2)
    omega = (pi.T.inv()*en).T

    # letzte Zeile der Jacobi-Matrix der Trafo:
    tn1 =  lie_deriv_covf(omega, f, args)

    return omega, tn1



# !!eigentlich lineare Theorie
def mimo_brunovsky(A, B, kappa_list):
    """
    kappa: List of controllability indices
    """
    A = np.matrix(st.to_np(A))
    B = np.matrix(st.to_np(B))

    assert A.shape[0] == A.shape[1]
    assert len(kappa_list) == B.shape[1]
    assert sum(kappa_list) == A.shape[0]

    columnlist = []
    for i,kappa in enumerate(kappa_list):
        b = B[:, i]
        for k in range(kappa):
            columnlist.append(A**k*b)


    Q = np.hstack(columnlist)
    iQ = np.linalg.inv(Q)

    ckappa_list = np.cumsum(kappa_list)-1 # -1 because counting starts at 0
    rows = [iQ[ck, :] for ck in ckappa_list]

    T_rows = []
    for i,kappa in enumerate(kappa_list):
        r = np.matrix(rows[i])
        for k in range(kappa):
            T_rows.append(r*A**k)

    T = np.vstack(T_rows)

    return T


if __name__ == '__main__':

    xx = sp.Matrix([x1,x2])

    f = sp.Matrix([x2, x1**2])
    g = sp.Matrix([1,1])


    w, tn1 = nl_acker_test(f, g, [x1,x2])

    IPS()