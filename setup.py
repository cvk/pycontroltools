from distutils.core import setup

setup(name='pycontroltools',
    version='0.1',
    packages=['pycontroltools'],
    requires=['numpy',
                'sympy',
                'scipy',
                'matplotlib'],
    )