# -*- coding: utf-8 -*-
"""
Created on Tue Jan 07 14:26:18 2014

@author: Chris Penndorf

Test for poleplacement.
"""

import poleplacement
import sympy as sp
import numpy as np
import unittest
from symb_tools import to_np

class placeSISOBadInput(unittest.TestCase):
    
    #example state matrix
    # n = 2
    A2_tuple = ([13,2],
                [13,25])
    A2_list = [(13,2),
               (13,25)]
    A2_bad = sp.Matrix([['x',2],
                        [13,25]])
    A2 = sp.Matrix([[13,2],
                    [13,25]])
    # n = 3
    A3 = sp.Matrix([[13,2,34],
                    [13,25,3],
                    [1,2,63]])
        
    # n = 4, rank decrease = 2
    A42 = sp.Matrix([[13,2,34,4],
                     [1,2,3,4],
                     [1,2,3,4],
                     [1,2,3,4]])
    
    # example input vector
    b2_tuple  = ([2,2]) # (n,m) = (2,1)
    b2_list  = [(2,2)] # (n,m) = (2,1)
    b2_bad  = sp.Matrix(['x',2]) # (n,m) = (2,1)
    
    b2  = sp.Matrix([2,2]) # (n,m) = (2,1)
    b3  = sp.Matrix([2,2,4]) # (n,m) = (3,1)
    b24  = sp.Matrix([[2,7],[3,5],[2,9],[1,10]]) # (n,m) = (2,4)
    b4  = sp.Matrix([2,2,4,4]) # (n,m) = (4,1)
    
    # desired pole locations    
    pol_d2_tuple = (-2.,-1.) # n = 2
    pol_d2_bad = (['x', -1.])

    pol_d2 = ([-2.,-1.]) # n = 2
    pol_d3 = ([-2.,-1.,-3.]) # n = 3
    pol_d4 = ([-2.,-1.,-1.,-1.]) # n = 4
       
    # assembled parameters             
    wrongTypeA2 = ( A2_tuple, A2_list )
    wrongTypeb2 = ( b2_tuple, b2_list )
    
    wrongShape =    (   (A2, b24, pol_d2),
                        (A2, b4, pol_d2),
                        (A3, b2, pol_d3),
                        (A3, b24, pol_d3),
                        (A3, b4, pol_d3),
                    )
                           
    def testWrongType(self):
        """ placeSISO should fail with wrong type of A, b """
        self.assertRaises(AssertionError, poleplacement.placeSISO, self.A2_bad,
                          self.b2, self.pol_d2)
        self.assertRaises(AssertionError, poleplacement.placeSISO, self.A2,
                          self.b2_bad, self.pol_d2)
        for A in self.wrongTypeA2:
            self.assertRaises(TypeError, poleplacement.placeSISO, A, self.b2,
                              self.pol_d2)
        for b in self.wrongTypeb2:
            self.assertRaises(TypeError, poleplacement.placeSISO, self.A2, b,
                              self.pol_d2)
            
    def testWrongShape(self):
        """ placeSISO should fail with wrong shape of matrices A or b """
        for A, b, pol_d in self.wrongShape:
            self.assertRaises(ValueError, poleplacement.placeSISO, A, b, pol_d)
    
    def testNotControllable(self):
        """ placeSISO should fail with non-controllable system """
        self.assertRaises(ValueError, poleplacement.placeSISO,
                          self.A42, self.b4, self.pol_d4)
        
    def testBadPoles(self):
        """ placeSISO should fail with bad number/type of poles """
        self.assertRaises(ValueError, poleplacement.placeSISO, self.A2,
                          self.b2, self.pol_d3)
        self.assertRaises(TypeError, poleplacement.placeSISO, self.A2, self.b2,
                          self.pol_d2_bad)
                          
class KnownValues(unittest.TestCase):
        
    #example state matrix
    # n = 2
    A2 = sp.Matrix([[13,2],
                    [13,25]])
    # n = 3
    A3 = sp.Matrix([[13,2,34],
                    [13,25,3],
                    [1,2,63]])
    
    # example input vector    
    b2  = sp.Matrix([2,2]) # (n,m) = (2,1)
    b3  = sp.Matrix([2,2,4]) # (n,m) = (3,1)
    
    # desired pole locations    
    pol_d2 = ([-2.,-1.]) # n = 2
    pol_d3 = ([-2.,-1.,-3.]) # n = 3
    
    # Values calculated with function "place" in the "Control System Toolbox"
    # in MATLAB R2013a
    # http://www.mathworks.de/de/help/control/ref/place.html
    
    knownValues =   (
    (   [A2, b2, pol_d2], 
        np.array([[6.45652173913043],
                   [14.0434782608696]]),
        np.array([[86.9565217391344e-003, -26.0869565217391],
                   [86.9565217391344e-003, -3.08695652173914]]) ),
    (   [A3, b3, pol_d3], 
         np.array([[-368.249751738228],
                    [-363.140019861356],
                    [392.444885799799]]),
         np.array([[749.499503476456, 728.280039722712, -750.889771599598],
                    [749.499503476456, 751.280039722712, -781.889771599598],
                    [1473.99900695291, 1454.56007944542, -1506.77954319920]]) )
    )
    
    def testKnownValues(self):
        """ placeSISO should give known result with known input """
        for [A, b, pol_d], k, A2_sol in self.knownValues:
            res_k, res_A2 = poleplacement.placeSISO(A, b, pol_d)
            self.assertTrue(np.allclose(to_np(res_k), k,
                                        atol = 1e-08, rtol = 0))
            self.assertTrue(np.allclose(to_np(res_A2), A2_sol,
                                        atol = 1e-08, rtol = 0))
        
if __name__ == "__main__":
	unittest.main()