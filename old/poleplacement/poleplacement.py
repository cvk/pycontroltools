# -*- coding: utf-8 -*-
"""
Created on Sat Nov 23 14:38:08 2013

@author: Chris Penndorf
"""
import sympy as sp
import numpy as np
from symb_tools import to_np

# for output
import pprint as pp



""" initializing A, b and pol_d"""

#example state matrix

A = sp.Matrix([[13,2],[13,25]]) # n = 2
#A = sp.Matrix([[13,2,34],[13,25,3],[1,2,63]]) # n = 3

#A = sp.Matrix([[13,2,34,4],[13,25,3,4],[1,2,63,4],[15,2,3,4]]) # n = 4
#A = sp.Matrix([[13,2,34,4],[1,2,3,4],[1,2,3,4],[1,52,3,74]]) # n = 4, rank decrease = 1
#A = sp.Matrix([[13,2,34,4],[1,2,3,4],[1,2,3,4],[1,2,3,4]]) # n = 4, rank decrease = 2
#A = sp.Matrix([[1,2,3,4],[1,2,3,4],[1,2,3,4],[1,2,3,4]]) # n = 4, rank decrease = 3

#A = sp.Matrix([[12,2,34,4,5],[13,25,3,4,4],[1,2,63,4,2],[15,2,3,4,8],[12,2,33,5,6]]) # n = 5
#A = np.array([[13,2,34,4,5],[13,25,3,4,4],[1,2,63,4,2],[15,2,3,4,8],[12,2,33,5,6]]) # n = 5


# example input vector

b  = sp.Matrix([2,2]) # (n,m) = (2,1)
#b  = sp.Matrix([2,2,4]) # (n,m) = (3,1)
#b  = sp.Matrix([2,2,4,4]) # (n,m) = (4,1)
#b  = sp.Matrix([2,2,4,4,5]) # (n,m) = (5,1)
#b  = sp.Matrix([[2,7],[3,5],[2,9],[1,10]]) # (n,m) = (2,4)


# desired pole locations

pol_d2 = ([-2.,-1.]) # n = 2
#pol_d = sp.Matrix([-2.,-1.,-3.]) # n = 3
#pol_d = sp.Matrix([-2.,-1.,-1.,-1.]) # n = 4
#pol_d = ([-2.,-1.,-1.,-1.,-3.]) # n = 5


def placeSISO(A, b, pol_d, output = False):
    """
        Calculates and returns the feedback vector k
        and the state matrix for the closed loop: (A - b*k.T)
        to stabilize a linear SISO system (m = 1)

        parameters:
        ===========
        A : sympy.MatrixBase
            state matrix of the open loop system

        b : sympy.MatrixBase
            input vector

        pol_d : list or tuple
             list of desired poles
            
        output : bool
            defines if there is console output
            
                
        return:
        ======
        k_sol : sympy.matrices.dense.MutableDenseMatrix
            calculated feedback vector for desired poles
            
        A2_sol : sympy.matrices.dense.MutableDenseMatrix
            state matrix for the closed loop

    """
    # error treatment
    # (check if inputs have correct type, all the matrices have the correct shape
    # and A is controllable)

    if not isinstance(A, sp.MatrixBase):
        raise TypeError("Matrix A has to be of type sympy.MatrixBase")

    if not isinstance(b, sp.MatrixBase):
        raise TypeError("Vector b has to be of type sympy.MatrixBase")
    
    n1, n2 = A.shape
    assert n1 == n2
    n = n2
    m = 1 # nur SISO-Systeme
    
    if not (b.shape == (n, m)):
        raise ValueError("""Input vector has to contain %d elements
                in shape (%d , %d)\n""" %(n*m, n, m))
    
    # check for controllability with Kalman criterion
    if not (kalmanControlCriterion(A,b) == True):
        raise ValueError("""System is not controllable!\n""")

    if not (len(pol_d) == n):
        raise ValueError("You have to give %d desired pole places!\n" %(n))

    # initializing feedback vector #2
    k = sp.symbols('k1:%d'%(n2+1))
    k = sp.Matrix(k)
    
    # calculating closed loop characteristical polynomial as a function of k

    s = sp.Symbol('s')
    I = sp.eye(n) # identity matrix

    A2 = s*I - (A - b*k.T)

    poly_A2 = sp.Poly(A2.det().expand(), s, domain="EX") # A2 as polynomial


    # calculating desired CLCP
    # creating desired CLCP out values of the desired poleplacement
    CLCP_d = 1
    for p in pol_d:
        CLCP_d *= (s - p)

    CLCP_d = sp.Poly(CLCP_d.expand(), s, domain="EX") # CLCP_d as polynomial

    # calculating difference between desired polynomial
    # and the polynomial depending on k-values
    diff_poly = poly_A2 - CLCP_d

    # solve the linear equation system for variables of k
    sol = sp.solve(diff_poly.as_dict().values(), k.values())

    A2_sol = -A2.subs(sol) + s*I # calculating A2 by substitution of solution values
    k_sol = k.subs(sol)

    # for checking the new poles
    # converting to numpy array with to_np
    A2_sol_np = to_np(A2_sol)

    A2_eig = np.linalg.eigvals(A2_sol_np)


    # OUTPUT
    if output == True:
        outputPlaceSISO(A2_sol, A, b, k, sol, A2_eig)
    
    # return statement
    return k_sol, A2_sol


def kalmanControlCriterion(A, B):
    """
        Uses the Kalman criterion to decide if a linear system
        is controllable
        
        parameters:
        ===========
        A : sympy.MatrixBase
            state matrix of the open loop system

        B : sympy.MatrixBase
            input matrix
            
        return:
        =======
        controllable : bool
            true = system is controllable\n
            false = system is not controllable
    """
    
    if not isinstance(A, sp.MatrixBase):
        raise TypeError("Matrix A has to be of type sympy.MatrixBase")

    if not isinstance(B, sp.MatrixBase):
        raise TypeError("Vector b has to be of type sympy.MatrixBase")
    
    n1, n2 = A.shape
    assert n1 == n2
    n = n2
    
    Qs = B    
    for ncount in range(1,n):
        MatAppend = A**(ncount) * B
        Qs = sp.Matrix.hstack(Qs, MatAppend)
    
    if (Qs.rank() == n):
        controllable = True
    else:
        controllable = False
        
    return controllable


def outputPlaceSISO(A2_sol, A, b, k, sol, A2_eig ):
    """
    gives an console output for the function placeSISO
    
    parameters:
    ==========
    outTuple : tuple
        (output value, description)
        
    """
    
    print "\n"
    print "----------------------------------------------------------------------"
    print "----------------------------------------------------------------------"
    print "POLEPLACEMENT for n-dimensional state systems\n"

    # for A2: creating float numbers with precision 2 for better printing
    A2_out = sp.Matrix(A2_sol)      # creating a copy of A2_sol
    for i,x in enumerate(A2_out):
        A2_out[i] = '{:.2f}'.format(float(x))


    print "given state matrix A:\n"
    pp.pprint(A)
    print
    print "given input vector b:\n"
    pp.pprint(b)
    print
    print "shape of feedback vector k:\n"
    pp.pprint(k)
    print
    print "calculated feedback constants k:\n"
    pp.pprint(sol)
    print
    print "calculated feedback state matrix A2:\n"
    pp.pprint(A2_out)
    print
    print "CHECK in numpy:\n"
    pp.pprint(A2_eig)
    print

    if (np.all(A2_eig <0)):
        print "All poles are negative.\n"
        print "The controlled system is stable!"

    print "\n"
    print "----------------------------------------------------------------------"
    print "----------------------------------------------------------------------"
