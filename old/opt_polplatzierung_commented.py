# -*- coding: utf-8 -*-
"""


19.02.2014:

Problem:

    given n m-dimensional supspaces S1, ... Sn of Rn, we search
    n unit vectors v1 \in S1, ... vn \in Sn such that det(v1, ..., vn)
    has a maximum


Solution:
    QR-Factorization of the spaces Si (each given by a (n,m)-matrix)

    -> new orthonormal matrices Qi

    parameterizing vi via m-dimensional spherical coordinates with r = 1

    defining a function mapping from n*(m-1) sperical coordinates to the
    determinant

    application of fmin


2nd Solution:
    after QR-Factorization: chose one sample v_i such that
    V = (v_1,...,v_n) is regular.

    Walk through the columns of V

    Fo column vj calculate the 1-dim. basis of the annihilator of all the other
    columns, say aj

    Project aj to Sj and replace vj with that new column

"""

"""
19.03.2014:

    Nachvollzug der Beispiele im neuen Buch

"""


import sympy as sp
import numpy as np
import scipy.optimize as opt

import symb_tools as st
#chris
import numtools_bearbeitet as nt

#chris
from pprint import pprint
#import scipy
from ipHelp import IPS, ip_syshook, sys#,Tracer
# Tracer(colors='Linux')() # start debugging



#chris: m == Dimension des sphärischen Vektors in Kugelkoordinaten
def generate_spherical_coordinate_vector(m):
    """
    h:=m-1
    returns Matrix([cos(a0), sin(a0)*cos(a1), ..., sin(ah*...*sin(a0)])

    # see:
    http://de.wikipedia.org/wiki/Kugelkoordinaten#Verallgemeinerung_auf_n-dimensionale_Kugelkoordinaten
    """

    # angles:
    aa = sp.symbols('a1:%i' % (m))
    assert len(aa) == m-1

    coords = []
    for i in range(m):
        cos_terms = [1]
        sin_terms = [1]
        if i<m-1:
            cos_terms = [sp.cos(aa[i])]
        if i > 0:
            sin_vars = aa[:i]
            sin_terms = [sp.sin(a) for a in aa[:i]]

        res_product = sp.Mul(*(cos_terms+sin_terms))


        coords.append(res_product)

    scv = sp.Matrix(coords)

    fnc = sp.lambdify(aa, coords, modules = "numpy")

    return fnc


def generate_optnfunc(Q_list):


    n = len(Q_list)
    m = Q_list[0].shape[1]

    g = generate_spherical_coordinate_vector(m)

    tmp_V = np.zeros((n,n)) # this will serve as temporary Matrix

    def opt_func(args, debug = False):
        assert len(args) == n*(m-1)
        arg_arr = np.array(args).reshape(n, -1)

        for i in range(n):
            aa = arg_arr[i, :]
            sphere_vector = g(*aa)
            subspace_vector = np.dot(Q_list[i], sphere_vector)
            tmp_V[:, i] = subspace_vector

        d = np.linalg.det(tmp_V)

        return -d**2

    opt_func.work_matrix = tmp_V

    return opt_func


# - - - - - - - 2nd Solution - - - -


def ortho_complement(M):
    """
    gets a n,n-matrix M which is assumed to have rank n-1
    returns: a column v with v.T*M = 0 and v.T*v = 1
    """

    dtype = M.dtype

    M = sp.Matrix(M)
    n, m = M.shape
    assert n == m
    assert M.rank() == n-1

    v = M.T.nullspace()[0]
    v = np.array(np.array(v), dtype = dtype).squeeze()
    v/= np.linalg.norm(v)
    return v


def exchange_all_cols(V, P_list):

    n = V.shape[0]
    for i in range(n):
        v = V[:,i]
        V[:, i] = v*0
        
        #chris: v2 steht senkrecht auf restlichen Vektoren
        v2 = ortho_complement(V)
        #chris: v2_projected ist die Projektion von v2 in den zugehörigen
        #       Vektorraum (v muss laut bedingung in Vektorraum liegen)
        v2_projected = np.dot(P_list[i], v2)

        norm = np.linalg.norm(v2_projected)
        assert not norm == 0
        v2_projected /= norm #chris: normierter projezierter Vektor

        V[:, i] = v2_projected

    return V


def check_linear_dependence(cols, spaces, details = False):

    res = []
    for c, s in zip(cols, spaces):
        chk = np.column_stack((s, c))
        m = s.shape[1]
        res.append(np.linalg.matrix_rank(chk)-m)

    if details:
        return sum(res) == 0, res
    else:
        return sum(res) == 0



def full_qr(A, only_null_space=False):
    """
    performs the qr numpy decomposition and augments q by its transposed
    null space (such that q_new is quadratic and regular)
    """
    #chris: Orthogonal-Dreiecks-Faktorisierung (aus dem linalg-Paket)
    #       der Matrix A, so dass A = Q*R
    #       q ist orthonormal   (== orthogonal unit vectors, real entries)
    #                           (q^(-1) == q.T)
    #       r ist obere Dreiecksmatrix
    q, r = np.linalg.qr(A)
    n1, n2 = q.shape

    if n2 < n1: #chris: falls mehr Spalten als Zeilen
        #chris: Nullraum der Matrix q (lineare Abbildung) == Kern von q
        #       q(x) = q*(x1,...,xn).T = 0
        N = nt.null(q).T
    
    #chris: erweitern von q mit dem transponierten Nullraum von q
    #       N verwendet obwohl nur bei n2<n1 gegeben ???
    Q = np.hstack((q, N))
    
    #chris: Q sollte jetzt quadratisch und regulär sein
    n1, n2 = Q.shape
    assert n1 == n2

    if only_null_space:
        return N

    return Q, r



np.random.seed(105)


n = 5
m = 3

S_list = []
Q_list = []
R_list = []
P_list = [] # Projektor-Matrix


# Bsp: 8.8:

A = np.array([[1, 1, 0, 0, 0],
              [0, 1, 1, 0, 1],
              [-1, 1, 1, 0, -1],
              [0, 0, 1, 1, 1],
              [1, -1, 0, 0, 2],])

B = np.array([[1, 2, 1, 1, 1],
              [-1, -1, -1, 0, 1],
              [-2, -1, -1, -1, 1]]).T

kk = sp.symbols("k1:6")
s = sp.symbols("s")
I = sp.eye(5)

#chris: Reglermatrix mit unbekannten Reglerparametern k1,...,k5
#       und ausgewählten m-1 Zeilen der reduzierten Bk
Bk = sp.Matrix([[-1, -2, -1, -2, -1],
              [2, 1, 2, 1, 2],
              kk])

#chris: Überprüfung der Bedingung (8.57) ???
#chris ---------------Prüfung von (8.57)-----------------------------              
              
Bk_red = Bk[0:-1, :]
Ak_red = sp.eye(3)[0:-1, :]

A_ = sp.Matrix((s*I - A))
B_ = sp.Matrix(B)

AB_ = sp.Matrix(np.hstack((A_,B_)))
BA_red = sp.Matrix(np.hstack((Bk_red,Ak_red)))

Matrix = sp.Matrix(np.vstack((AB_,BA_red)))

#print scipy.rank(Matrix)

#chris ----------------------------------------------------------------------              

A2 = A+ B*Bk

#chris: EW in Diagonalmatrix
Lambda = np.diag([-5, -4, -3, -2, -1])

#chris: Aufstellen des LAG
#       det(s*I - A - B*Bk)
d1 = (s*I-A2).det()
#       det(s*I - diag(gewünschte EW))
d2 = (s*I-Lambda).det()

#chris: zu lösendes LAG
eqns = sp.Matrix (st.coeffs(d1, s)) - sp.Matrix(st.coeffs(d2, s))

#chris: sol == Vektor der Reglerparameter k1,...,k5
#       aus Lösung des LAG (S.456)
sol = sp.solve(eqns, kk)

A2num = st.to_np(A2.subs(sol))
#chris: foo == Überprüfung der EW
#       V == zugehörige Eigenvektoren (EV)
foo, V = np.linalg.eig(A2num)
#chris: det(V) darf nicht verschwinden, da Spaltenvektoren von V
#       linear unabhängig

# Überprüfung des QR Beispiels (8.9)

Qb, Rb = full_qr(B)
Qb2 = full_qr(B, only_null_space=True) # "Uh" #chris: Nullraum von q
if 0:
    print Qb
    print Rb #chris: keine 0-Zeilen  ???


# Unterräume berechnen, in denen der jeweilige EV liegen darf:
# (mittels QR-Zerlegung)

Sh_list = []    #chris: überflüssig ???
V0 = np.zeros((n,n))

#chris: für EW_i und Vektorraum von EV_i (i = 1,...,n)
for i, s_i in enumerate(np.diag(Lambda)):
    #chris: dot-product for the qr-factorization (8.73)
    Atmp = np.dot( (A - s_i*np.eye(5)).T, Qb2)
    
    #chris: Nullräume für EVs in Liste
    Sh = full_qr(Atmp, only_null_space=True)
    S_list.append(Sh)# stimmen mit den Angaben in Bsp. 8.10 überein
    
    #chris: der erste EV des Vektorraumes i in EV-Matrix V0
    #ÜBERFLÜSSIG    
    V0[:,i] = Sh[:, 0] # construct first Eigenvector-Matrix


    #ÜBERFLÜSSIG (Q= Sh sollte ebenfalls funktionieren)
    Q, R = np.linalg.qr(Sh) # QR-Factorization

    Q_list.append(Q)
    R_list.append(R)
    
    #chris: warum hier nicht full_qr anwenden ???
    P_list.append(np.dot(Q, Q.T))

#chris: in Bsp. 8.8 berechnete Determinante von V
#       (mit Reglerparametern aus Abschn. 8.5.3. berechnet)
#V = V0*1
print np.linalg.det(V)

# Optimierung durchführen

for i in range(6): # im Buch: 6 Schritte (Ergebnisse stimmen bis auf Rundungsfehler)
    V = exchange_all_cols(V, P_list)    
    print i, np.linalg.det(V)
V1 = V*1

if 0:
    # andere Startwerte -> Verfahren konvergiert gegen gleiches V (nach 20 Schr.)
    V = V0*1
    for i in range(20):
        V = exchange_all_cols(V, P_list)
        print i, np.linalg.det(V)
    V2 = V*1


# Berechnung der Regler-Matrix:
Rinv = np.linalg.inv(Rb)
Vinv = np.linalg.inv(V)
Qb1 = Qb[:, :B.shape[1]] # "Uv"
A_new = nt.dd(V, Lambda, Vinv)
Bk_new = nt.dd(Rinv, Qb1.T, (A_new - A))

#chris
pprint(Bk_new)

IPS()
