# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 09:48:42 2014

Functions from symb_tools.py concerning Lie algebra

@author: Carsten Knoll

Edits&Comments: Chris Penndorf
"""
import sympy as sp

#chris:
from IPython import embed

#chris: Berechnet Jacobi-Matrix mit jacobian() aus sympy-Modul
#       + wandelt expr, falls kein length-Attribut vorhanden, in list um
#       *args:  muss Liste sein,
#               Liste der Variablen, nach denen abgeleitet werden soll
#       expr: kann Funktion/Liste/... sein
def jac(expr, *args):
    if not hasattr(expr, '__len__'):
        expr = [expr]
    return sp.Matrix(expr).jacobian(args)

#chris: ((L_f)^n)*h mit  ((L_f)^0)*h = h
#       Lie-Ableitung eines Skalarfeldes h (== sf) == Funktion
#       entlang eines Vektorfeldes f (== vf) == Liste/Spaltenvektor
#       == Richtungsableitung der Funktion h nach f(x)
#       n... Anzahl Ableitungen (ganzzahlig größer 0)
#       x... Vektoren, die Basis des Vektorfeldes aufspannen (nach denen abgeleitet wird)
def lie_deriv(sf, vf, x, n = 1):
    """
    lie_deriv of a scalar field along a vector field
    """
    #chris: falls Basis nicht als Liste sondern als Matrix (Vektor) angegeben
    
#==============================================================================
#   chris:  nochmal prüfen, ob das raus kann!
#           denn jac()  nimmt sowohl Zeilen- als auch Spaltenvektor als args
    if isinstance(x, sp.Matrix):
        assert x.shape[1] == 1  #chris: vorraussetzen, dass Basis
                                #       als Spaltenvektor gegeben
                                #!      list würde auch mit Zeilenvektor
                                #       funktionieren
        x = list(x) #chris: in Liste umwandeln 
#==============================================================================

    assert int(n) == n and n >= 0 #chris: Anzahl Ableitung ganzzahlig größer 0
    if n == 0: #chris: ((L_f)^0)*h = h
        return sf
        
    #chris: (Jacobi-Matrix == Richtungsableitung des Skalarfeldes)*Vektorfeld
    res = jac(sf, x)*vf
    assert res.shape == (1,1)   #chris: Voraussetzung,
                                #       wenn alle Parameter korrekte Form hatten
    res = res[0] #chris: Lösung als Funktion

    if n > 1:   #chris: falls höhere Lie-Ableitung --> n-1te Lie-Ableitung von res
                #       ((L_f)^n)*h = ((L_f)^(n-1))*((L_f)*h)
        return lie_deriv(res, vf, x, n-1)
    else:
        return res  #chris: fertig, wenn n-mal durchlaufen

#chris: [f,g](x) == (dg/dx)*df(x) - (df/dx)*dg(x) == (ad_f)g(x)
#       mit ((ad_f)^0)g(x) = g(x)
#       Lie-Klammer zweier Vektorfelder f(x) & g(x)
#       == Lie-Ableitung eines Vektorfelds g(x) durch ein Vektorfeld f(x)
#       x... Vektoren, die Basis des Vektorfeldes aufspannen (nach denen abgeleitet wird)
def lie_bracket(f, g, *args, **kwargs):
    """
    f, g should be vectors (or lists)

    call possibillities:

    lie_bracket(f, g, x1, x2, x3)
    lie_bracket(f, g, [x1, x2, x3])
    lie_bracket(f, g, sp.Matrix([x1, x2, x3]) )



    optional keyword arg n ... order
    """

    assert len(args) > 0    #chris: Basis der Vektorfelder muss gegeben sein

#==============================================================================
#     chris:  wenn args als Matrix gegeben, dann als Spaltenvektor
#             nochmal prüfen, ob das raus kann!
#             denn sympy.jacobian()  nimmt sowohl Zeilen-
#             als auch Spaltenvektor als args
    if isinstance(args[0], sp.Matrix):
        assert args[0].shape[1] == 1 
        args = list(args[0]) #chris: umformen in Liste
#==============================================================================

    if hasattr(args[0], '__len__'):
        args = args[0] #chris: entpacken falls nochmals gepackt (in Matrix etc) übergeben
        
    #chris: keyword args müssen mit Schlüsselwort übergeben werden (dict)
    n = kwargs.get('n', 1) # wenn n nicht gegeben, dann n=1

    if n == 0:
        return g

    assert n > 0 #and isinstance(n, int)
    assert len(args) == len(list(f))

    # Umwandeld in sympy-Matrizen
    f = sp.Matrix(f)
    g = sp.Matrix(g)

    jf = f.jacobian(args)
    jg = g.jacobian(args)


    res = jg * f - jf * g

    if n > 1: #chris: falls höhere Lie-Klammern --> n-1te Lie-Klammer von res
        res = lie_bracket(f, res, *args, n=n-1)

    return res

#chris: Lie-Ableitung Kovektor w(x) (Zeilenvektor!) entlang Vektorfeld
def lie_deriv_covf(w, f, args, **kwargs):
    # transpose_jac ... Keyword für das Transponieren, der Jacobimatrix von w
    """
    Lie derivative of covector fields along vector fields

    w, f should be 1 x n and n x 1 Matrices


    (includes the option to omit the transposition of Dw
    -> transpose_jac = False)
    """
    
    #chris: Form der Vektoren überprüfen
    k,l = w.shape
    m, n = f.shape
    assert  k==1 and n==1
    assert l==m

#==============================================================================
#      chris:  wenn args als Matrix gegeben, dann als Spaltenvektor
#              nochmal prüfen, ob das raus kann!
#              denn sympy.jacobian()  nimmt sowohl Zeilen-
#              als auch Spaltenvektor als args
    if isinstance(args[0], sp.Matrix):
        assert args[0].shape[1] == 1
        args = list(args[0])
#==============================================================================

    if hasattr(args[0], '__len__'):
        args = args[0]

    assert len(args) == len(list(f))    #chris: Check, ob alle Argumente
                                        #       (Ableitungsvariablen) gegeben

    n = kwargs.get('n', 1) # wenn n nicht gegeben, dann n=1

    if n == 0:
        return w

    assert n > 0 #and isinstance(n, int)


    # caution: in sympy jacobians of row and col vectors are equal
    # -> transpose is needless (but makes the formula consistent with books)
    jwT = w.T.jacobian(args)

    jf = f.jacobian(args)


    #chris: ohne Transponieren der Jacobimatrix von w
    if kwargs.get("transpose_jac", True) == False:
        # stricly this is not a lie derivative
        # but nevertheless sometimes needed
        res = w*jf + f.T * jwT
    else:

        # This is the default case :
        res = w*jf + f.T * jwT.T

    if n > 1:
        res = lie_deriv_covf(res, f, args, n = n-1)

    return res
    
#chris:
embed()