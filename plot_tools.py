# -*- coding: utf-8 -*-

from __future__ import  division

import numpy as np
from numpy import sin, cos, tan, exp, pi

import pylab as pl

from ipHelp import IPS, ip_syshook, ip_extra_syshook, ST
ip_syshook(1)

import numpy as np


"""
adapted from
http://www.scipy.org/Cookbook/Matplotlib/Show_colormaps
"""
from pylab import matplotlib, pcolor, colorbar, show, imshow

def make_cmap_rwg(data, mid = None):
    
    """creates a colormap which is adapted to data:
    the 'middle'-value can be specified and will occur white,
    greater values will occur green, smaller ones red
    
    -> rwg =  red white green
    """   
    
    a = np.min(data.flatten())
    if mid == None:
        b = (c-a)*.5
    else:
        b = mid
    c = np.max(data.flatten())
    
    assert a < b < c
    
    y = (b-a) / (c-a)
    x = .5 * y 
    z = 1 - .5 * (c-b)/(c-a)
    
    assert 0 < x < y < z <1
    
        
    cdict ={          
            'red': ((0.0, 0, 1),
                       (x,1, 1),
                       (y, 1, 1),
                       (z, 0.5, 0.5),
                       (1.0, 0.0, 0.0)),
            
            'green':  ((0.0, 0.0, 0.0),
                       (x,0.5, 0.5),
                       (y, 1, 1),
                       (z,1.0, 1.0),
                       (1.0, 1, 1.0)),
    
             
             'blue':  ((0.0, 0.0, 0.0),
                       (x, 0.5, 0.5),
                       (y, 1, 1),
                       (z,0.5, 0.5),
                       (1.0, 0.0, 0.0))
            }
    
    my_cmap = matplotlib.colors.LinearSegmentedColormap('my_colormap',cdict,256)
    return my_cmap
        


if __name__ == '__main__':
    
    rand = np.random.rand
    data = rand(10,10) * 3 - rand(10,10) 
    
    my_cmap = make_cmap_rwg(data, mid = 0)
    #pcolor(data,cmap=my_cmap)
    imshow(data,cmap=my_cmap)
    colorbar()
    
    
    show()



#cdict = {'red': ((0.0, 0.0, 0.0),
#                 (0.5, 1.0, 0.7),
#                 (1.0, 1.0, 1.0)),
#         'green': ((0.0, 0.0, 0.0),
#                   (0.5, 1.0, 0.0),
#                   (1.0, 1.0, 1.0)),
#         'blue': ((0.0, 0.0, 0.9),
#                  (0.5, 1.0, 0.0),
#                  (1.0, 0.5, 1.0))}
#
#
#cdict ={'red':  ((0.0, 0.0, 0.0),
#                   (0.25,0.0, 0.0),
#                   (0.5, 0.8, 1.0),
#                   (0.75,1.0, 1.0),
#                   (1.0, 0.4, 1.0)),
#
#         'green': ((0.0, 0.0, 0.4),
#                   (0.25,0.8, 0.8),
#                   (0.5, 0.9, 0.9),
#                   (0.75,0.0, 0.0),
#                   (1.0, 0.0, 0.0)),
#
#         'blue':  ((0.0, 0.0, 0.0),
#                   (0.25, 0, 0),
#                   (0.5, 0.8, 0.8),
#                   (0.75,0.0, 0.0),
#                   (1.0, 0.0, 0.0))
#        }



