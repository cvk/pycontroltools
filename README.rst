.. pycontroltools__sphinx_README-label:

About
=====

**pycontroltools** is a Python toolbox with algorithms for linear and nonlinear control theory.

It is developed at Dresden University of Technology at the 
`Institute for Control Theory`__ (RST).

__ University_
.. _University: http://www.et.tu-dresden.de/rst/

Latest development was done by Chris Penndorf, supervised by Carsten Knoll.
(The current status of this toolbox is still 'experimental code'. It will be extended in the future.)

Usage
=====

To use the toolbox, simply copy the folder **pycontroltools**
from this repository to your local workspace and make sure to include the path
in your **PYTHONPATH**.
	
Documentation
=============

The documentation can be found at http://pycontroltools.readthedocs.org/en/latest/ .


See also
========

Further software devolped by or in cooperation with our institute can be found here: http://tu-dresden.de/rst/software .

You are maybe also interested in the control systems library maintained by Richard M. Murray and others: https://github.com/python-control/python-control .